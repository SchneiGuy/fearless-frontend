const url = "http://localhost:8000/api/conferences/";

document.addEventListener("DOMContentLoaded", async () => {
  const response = await fetch(url);
  if (response.ok) {
    const data = await response.json();
    const conferenceSelect = document.getElementById("conference");
    for (const { id, name } of data.conferences) {
      conferenceSelect.innerHTML += `<option value="${id}">${name}</option>`;
    }
  } else {
    console.log("Error");
  }

  const form = document.getElementById("create-presentation-form");
  form.addEventListener("submit", async (event) => {
    event.preventDefault();

    const formData = new FormData(form);

    const data = Object.fromEntries(formData);
    const presentationsUrl = `http://localhost:8000/api/conferences/${data.conference}/presentations/`;
    delete data.conference;
    const response = await fetch(presentationsUrl, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(data),
    });
    if (response.ok) {
      form.reset();
      const newPresentation = await response.json();
      console.log(newPresentation);
    } else {
      console.log("Error submitting form");
    }
  });
});
