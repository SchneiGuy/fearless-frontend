function createCard(title, description, pictureUrl, starts, ends, location) {
  return `
    <div class="col-4">
      <div class="card shadow">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">${starts} - ${ends}
      </div>
    </div>
    `;
}

function createPlaceholder(index) {
  return `
    <div id="placeholder-${index}" class="col-4">
      <div class="card shadow" aria-hidden="true">
        <img src="https://via.placeholder.com/350" class="card-img-top placeholder" alt="placeholder">
        <div class="card-body placeholder-glow">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
            </h5>
            <h6 class="card-subtitle placeholder-glow">
                <span class="placeholder col-6"></span>
            </h6>
            <p class="card-text placeholder-glow">
              <span class="placeholder col-7"></span>
              <span class="placeholder col-4"></span>
              <span class="placeholder col-4"></span>
            </p>
        </div>
        <div class="card-footer text-muted placeholder"></div>
      </div>
    </div>`;
}

function displayError(message, gridStyle = "") {
  return `
    <div class="${gridStyle}">
      <div class="alert alert-danger" role="alert">${message}
      </div>
    </div>
    `;
}

function formatDate(dateString) {
  return new Date(dateString).toLocaleDateString();
}

window.addEventListener("DOMContentLoaded", async () => {
  const url = "http://localhost:8000/api/conferences/";
  const column = document.querySelector(".row");
  try {
    const response = await fetch(url);
    if (response.ok) {
      const { conferences } = await response.json();
      for (const [index, conference] of conferences.entries()) {
        console.log(conference);
        column.innerHTML += createPlaceholder(index);
        const detailUrl = `http://localhost:8000/${conference.href}`;
        const detailResponse = await fetch(detailUrl);
        if (detailResponse.ok) {
          const details = await detailResponse.json();
          console.log(details);
          const {
            name,
            description,
            location: { picture_url, name: location },
            starts,
            ends,
          } = details.conference;

          const placeholder = document.getElementById(`placeholder-${index}`);
          placeholder.remove();

          column.innerHTML += createCard(
            name,
            description,
            picture_url,
            formatDate(starts),
            formatDate(ends),
            location
          );
        } else {
          const placeholder = document.getElementById(`placeholder-${index}`);
          placeholder.remove();
          column.innerHTML += displayError(
            `Attempt to get details for ${conference.name} failed`,
            "col-4"
          );
        }
      }
    } else {
      column.innerHTML += displayError(
        "Attempt to get conferences from server failed"
      );
    }
  } catch (error) {
    column.innerHTML += displayError(error);
  }
});
